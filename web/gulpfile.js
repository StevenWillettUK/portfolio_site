var gulp = require('gulp');
// var pug = require('gulp-pug');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');


// Check code quality
// Lint Task
gulp.task('lint', function() {
    return gulp.src('assets/src/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});


// gulp.task('html', function(){
//     return gulp.src('client/templates/*.pug')
//         .pipe(pug())
//         .pipe(gulp.dest('build/html'))
// });

gulp.task('sass', function() {
    return gulp.src('assets/src/scss/*.scss')
        .pipe(sass())
        .pipe(concat('gulped.min.css'))
        .pipe(gulp.dest('assets/dist/css'));
});

gulp.task('js', function(){
    return gulp.src('assets/src/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('gulped.min.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/dist/js'))
});

gulp.task('watch', function() {
    gulp.watch('assets/src/js/*.js', ['lint', 'js']);
    gulp.watch('assets/src/scss/*.scss', ['sass']);
});

gulp.task('default', ['lint', 'sass', 'js', 'watch']); // 'html'