<!DOCTYPE html>
<html lang="en-GB">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Portfolio | Steven Willett</title>

    <!-- CORE META TAGS -->
    <meta name="description" content="Portfolio of works and demos from Steven Willett">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- OG TAGS -->
    <meta property="og:title" content="Steven Willett's Portfolio of works and examples.">
    <meta property="og:type" content="website">
    <meta property="og:image" content="http://portfolio.stevenwillett.com/assets/images/og/steven-willett-portfolio.png">
    <meta property="og:url" content="http://portfolio.stevenwillett.com">
    <meta property="og:description" content="">
    <!--<meta property="og:video" content="">-->
    <meta property="og:locale" content="en_GB">
    <meta property="og:site_name" content="Portfolio of works and demos from Steven Willett">
    <meta property="" content="">
    <meta property="" content="">

    <!-- CANONICAL URL -->
    <link rel="canonical" href="http://portfolio.stevenwillett.com" />

    <!-- ASYNC CSS -->
    <link rel="preload" href="assets/dist/css/gulped.min.css" as="style">

    <!-- DEFERRED SCRIPTS -->
    <script defer rel="script" src="assets/dist/js/gulped.min.js"></script>


</head>
<body>

<header clas="main-header">
    <nav>
        <ul>
            <li><a href="javascript:;">Menu Item</a></li>
        </ul>
    </nav>
</header>

<section>

    <article>
        <header>
            <h2>Heading title</h2>
            <p>Heading Supporting Content</p>
        </header>
        <p>Item Content</p>
    </article>

    <article>
        <header>
            <h2>Heading title</h2>
            <p>Heading Supporting Content</p>
        </header>
        <p>Item Content</p>
    </article>

</section>

<aside>
    <h2>Sideline</h2>
    <p>Some Content.</p>
</aside>

<footer>
    <p>Copyright &copy; <?= date('Y'); ?> Steven Willett</p>
</footer>


</body>
</html>